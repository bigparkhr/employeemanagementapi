package com.bg.employeemanagementapi.controller;

import com.bg.employeemanagementapi.model.EmployeeItem;
import com.bg.employeemanagementapi.model.EmployeeRequest;
import com.bg.employeemanagementapi.model.EmployeeResponse;
import com.bg.employeemanagementapi.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/people")
    public String setEmployee(@RequestBody EmployeeRequest request) {
        employeeService.setEmployee(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<EmployeeItem> getEmployees() {return employeeService.getEmployees();}

    @GetMapping("/detail/{id}")
    public EmployeeResponse getEmployee(@PathVariable long id) {
        return employeeService.getEmployee(id);
    }
}