package com.bg.employeemanagementapi.entity;

import com.bg.employeemanagementapi.enums.Department;
import com.bg.employeemanagementapi.enums.Duty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer employeeNumber;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Department department;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Duty duty;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false)
    private LocalDate dateOfJoiningTheCompany;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String mobile;

}
