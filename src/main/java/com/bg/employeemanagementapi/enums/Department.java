package com.bg.employeemanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Department {
    PERSONNEL("인사 부서"),
    GENERAL_AFFAIRS("총무 부서"),
    FACILITIES("시설 부서"),
    QUALITY("품질 부서"),
    PRODUCTION("생산 부서"),
    SALES("영업 부서"),
    DEVELOPMENT("개발 부서"),
    OWNER("사장님");

    private final String affiliatedDepartment;
}
