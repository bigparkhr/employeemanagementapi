package com.bg.employeemanagementapi.model;

import com.bg.employeemanagementapi.enums.Department;
import com.bg.employeemanagementapi.enums.Duty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeItem {
    private Integer employeeNumber;
    private String name;
    private String department;
    private String duty;
    private LocalDate birthDate;
    private LocalDate dateOfJoiningTheCompany;
    private String address;
    private String mobile;
}
