package com.bg.employeemanagementapi.model;

import com.bg.employeemanagementapi.enums.Department;
import com.bg.employeemanagementapi.enums.Duty;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeRequest {
    private Integer employeeNumber;
    private String name;

    @Enumerated(value = EnumType.STRING)
    private Department department;

    @Enumerated(value = EnumType.STRING)
    private Duty duty;

    private LocalDate birthDate;
    private LocalDate dateOfJoiningTheCompany;
    private String address;
    private String mobile;
}
