package com.bg.employeemanagementapi.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeResponse {
    private Long id;
    private Integer employeeNumber;
    private String name;
    private String department;
    private String duty;
    private LocalDate birthDate;
    private LocalDate dateOfJoiningTheCompany;
    private String address;
    private String mobile;
}
