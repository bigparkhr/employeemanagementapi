package com.bg.employeemanagementapi.service;

import com.bg.employeemanagementapi.entity.Employee;
import com.bg.employeemanagementapi.model.EmployeeItem;
import com.bg.employeemanagementapi.model.EmployeeRequest;
import com.bg.employeemanagementapi.model.EmployeeResponse;
import com.bg.employeemanagementapi.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public void setEmployee(EmployeeRequest request) {
        Employee addData = new Employee();
        addData.setEmployeeNumber(request.getEmployeeNumber());
        addData.setName(request.getName());
        addData.setDepartment(request.getDepartment());
        addData.setDuty(request.getDuty());
        addData.setBirthDate(request.getBirthDate());
        addData.setDateOfJoiningTheCompany(request.getDateOfJoiningTheCompany());
        addData.setAddress(request.getAddress());
        addData.setMobile(request.getMobile());

        employeeRepository.save(addData);
    }

    public List<EmployeeItem> getEmployees() {
        List<Employee> originList = employeeRepository.findAll();

        List<EmployeeItem> result = new LinkedList<>();

        for (Employee employee : originList) {
            EmployeeItem addItem = new EmployeeItem();
            addItem.setEmployeeNumber(employee.getEmployeeNumber());
            addItem.setName(employee.getName());
            addItem.setDepartment(employee.getDepartment().getAffiliatedDepartment());
            addItem.setDuty(employee.getDuty().getDutyClass());
            addItem.setBirthDate(employee.getBirthDate());
            addItem.setDateOfJoiningTheCompany(employee.getDateOfJoiningTheCompany());
            addItem.setAddress(employee.getAddress());
            addItem.setMobile(employee.getMobile());

            result.add(addItem);

        }

        return result;
    }

    public EmployeeResponse getEmployee(long id) {
        Employee originData = employeeRepository.findById(id).orElseThrow();

        EmployeeResponse response = new EmployeeResponse();
        response.setId(originData.getId());
        response.setEmployeeNumber(originData.getEmployeeNumber());
        response.setName(originData.getName());
        response.setDepartment(originData.getDepartment().getAffiliatedDepartment());
        response.setDuty(originData.getDuty().getDutyClass());
        response.setBirthDate(originData.getBirthDate());
        response.setDateOfJoiningTheCompany(originData.getDateOfJoiningTheCompany());
        response.setAddress(originData.getAddress());
        response.setMobile(originData.getMobile());

        return response;
    }
}
